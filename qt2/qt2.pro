QT += core
QT -= gui
QT += core sql

CONFIG += c++11

TARGET = qt2
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    menu.cpp \
    emp.cpp \
    cit.cpp \
    login.cpp \
    admin.cpp

HEADERS += \
    menu.hpp \
    emp.hpp \
    cit.hpp \
    login.hpp \
    admin.hpp
