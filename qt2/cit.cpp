#include <QCoreApplication>
#include <QtSql>
#include <iostream>

using namespace std;

#include "cit.hpp"

void show_cit(){
    qInfo() << "show_civ start";
    QSqlQuery query(db);
    query.prepare("SELECT pesel, first_name, last_name FROM citizens");
    query.exec();
    qInfo() << "PESEL" << "\t" << "imię" << "\t" << "nazwisko";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}


void show_inv(){
    qInfo() << "show_inv start";
    QSqlQuery query(db);
    query.prepare("SELECT case_number, last_name, description FROM investigations JOIN employees ON(officer=pesel)");
    query.exec();
    qInfo() << "Nr sprawy" << "\t" << "oficer" << "\t" << "opis";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}
