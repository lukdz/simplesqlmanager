#include <QCoreApplication>
#include <QtSql>
#include <iostream>

using namespace std;

#include "menu.hpp"
#include "emp.hpp"
#include "cit.hpp"


void show_op(){
    qInfo() << "\n";
    qInfo() << "-1 - zakończ";
    qInfo() << "0 - pomoc";
    qInfo() << "1 - wypisz zaturdnionych";
    qInfo() << "2 - wypisz policjantow";
    qInfo() << "3 - wypisz pracownikow cywilnych";
    qInfo() << "4 - wypisz obywateli";
    qInfo() << "5 - wypisz samochody";
    qInfo() << "6 - wypisz broń";
    qInfo() << "7 - wypisz wyposarzenie";
    qInfo() << "8 - wypisz psy policyjne";
    qInfo() << "9 - wypisz śledztwa";
}



void menu(){
    qInfo() << "menu start";
    show_op();
    int i;
    while(true){
        cin >> i;
        qInfo() << i;
        switch( i ){
            case -1:
                exit(0);
                break;
            case 0:
                show_op();
                break;
            case 1:
                show_emp();
                break;
            case 2:
                show_cops();
                break;
            case 3:
                show_civ();
                break;
            case 4:
                show_cit();
                break;
            case 5:
                show_cars();
                break;
            case 6:
                show_guns();
                break;
            case 7:
                show_eq();
                break;
            case 8:
                show_dogs();
                break;
            case 9:
                show_inv();
                break;
            default:
                cout << "nieprawidłowe polecenie, przoszę sprobuj jeszcze raz";
                break;
        }
    }
}
