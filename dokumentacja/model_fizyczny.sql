DROP TABLE employees CASCADE;
DROP TABLE cops CASCADE;
DROP TABLE civilians;
DROP TABLE investigations CASCADE; 
DROP TABLE cars;
DROP TABLE guns;
DROP TABLE equipment;
DROP TABLE citizens CASCADE;
DROP TABLE police_dogs;
DROP TABLE suspects;
DROP TABLE witnesses;


CREATE TABLE employees(
pesel text PRIMARY KEY,
first_name text,
last_name text,
login text NOT NULL,
password text NOT NULL,
phone_number text DEFAULT NULL,
address text,
permission_level int DEFAULT 10);

CREATE TABLE cops(
pesel text PRIMARY KEY,
rank text NOT NULL DEFAULT 'posterunkowy',
wage int);

CREATE TABLE civilians(
pesel text PRIMARY KEY,
position text 
NOT NULL CHECK (position LIKE 'm' OR position LIKE 'k' OR position LIKE  'p'),
wage int);

CREATE TABLE citizens(
id_ci SERIAL PRIMARY KEY,
pesel text,
first_name text,
last_name text,
telefon text DEFAULT NULL,
address text, 
description text);

CREATE TABLE investigations(
id_in SERIAL PRIMARY KEY,
case_number text,
date date DEFAULT current_date,
officer text REFERENCES cops(pesel) NOT NULL,
description text);

CREATE TABLE suspects(
id_ci SERIAL PRIMARY KEY,
suspect SERIAL NOT NULL REFERENCES citizens(id_ci), 
investigations SERIAL NOT NULL REFERENCES investigations(id_in), 
description text DEFAULT NULL);

CREATE TABLE witnesses(
id_ci SERIAL PRIMARY KEY,
witness SERIAL NOT NULL REFERENCES citizens(id_ci),
investigations SERIAL NOT NULL REFERENCES investigations(id_in), 
description text DEFAULT NULL);

CREATE TABLE cars(
vin int PRIMARY KEY NOT NULL,
reg_number text,
owner text REFERENCES employees(pesel)); 

CREATE TABLE guns( 
id_gu SERIAL PRIMARY KEY,
serial_number text NOT NULL,
model text,
brand text,
caliber text,
owner text REFERENCES cops(pesel));

CREATE TABLE equipment(
id_eq SERIAL PRIMARY KEY,
name text NOT NULL,
date_added date DEFAULT current_date, 
quantity int DEFAULT 0,
value int,
description text DEFAULT NULL);

CREATE TABLE police_dogs(
id_do SERIAL PRIMARY KEY,
name text NOT NULL,
year_of_birth integer, 
owner text REFERENCES cops(pesel),
description text DEFAULT NULL);







